#!/usr/bin/env bash

# curl -o arch-installer.sh -sL https://gitlab.com/Archx/arch-install-helper/-/raw/master/arch-installer.sh && bash arch-installer.sh

set -e

# Installer description:
#   Auszuführen als: root-user der live umgebung
#
#   Filesystem:      ext4
#   Disk Layout:     '/' only
#   Encrypted:       no
#   Swap:            no
#   Keymap:          de-latin1
#   LANG:            en_US.UTF-8
#   Locales:         [en_US.UTF-8 UTF-8] and [de_DE.UTF-8 UTF-8]
#   efi-directory:   /boot

# =============================== #
# ===== Checking User Input ===== #
# =============================== #
print_help() {
	echo "Please provide the following env variables:"
	echo "  USER_NAME         Name for a user who is granted root permissions"
	echo "  USER_PASSWORD     (Default: 0000) Password will be used for the created user"
	echo "  BLOCK_DEVICE      You can list blockdevices using 'lsblk'"
	echo "                    most commonly this is 'sda'"
	echo "                    ALL DATA WILL BE WIPED on this device!"
	echo ""
	echo "Exit"
}

[ "${USER_NAME}"     = "" ] && print_help && exit 1
[ "${BLOCK_DEVICE}"  = "" ] && print_help && exit 1
USER_PASSWORD=${USER_PASSWORD:-0000}

# ====================================== #
# ===== If the Block device exists ===== #
# ====================================== #
BLOCK_DEVICE_PREFIX_PATH=/dev/
AVAILABLE_BLOCK_DEVICES=$(lsblk -n | grep -P "^[a-zA-Z]{1,}" | awk '{print $1}')
SELECTED_BLOCK_DEVICE_EXISTS=$(echo -e "${AVAILABLE_BLOCK_DEVICES}" | grep "${BLOCK_DEVICE}")
if [ "${SELECTED_BLOCK_DEVICE_EXISTS}" != "${BLOCK_DEVICE}" ]; then
	echo "Block device [${BLOCK_DEVICE}] doesnt exist. Canceled execution."
	exit 1
fi
BLOCK_DEVICE_PATH=${BLOCK_DEVICE_PREFIX_PATH}${BLOCK_DEVICE}

# =================================== #
# ===== Print installer Options ===== #
# =================================== #
echo -e "USER_NAME:     ${USER_NAME}"
echo -e "USER_PASSWORD: ${USER_PASSWORD}"
echo -e "BLOCK_DEVICE:  ${BLOCK_DEVICE}"

# ================================================= #
# ===== Get confirmation for the installation ===== #
# ================================================= #
read -p "Are you sure? Press 'y' to continue: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo "Installation begins. Please do not interrupt the process."
else
	echo "Exit"
	exit 0
fi

# ========================================= #
# ===== Prepare and Check environment ===== #
# ========================================= #
loadkeys de-latin1
timedatectl set-ntp true

# ============================= #
# ===== Create Partitions ===== #
# ============================= #
wipefs -a -f "${BLOCK_DEVICE_PATH}"
# Format:
#   Device:          ${BLOCK_DEVICE_PATH}
#   Partition Table: GPT
#   Partition 1:     512M, EFI
#   Partition 2:     remaining space, linux root
FDISK_CREATE_GPT_TABLE=g
FDISK_CREATE_PARTITION=n
FDISK_ACCEPT_DEFAULT=
FDISK_CHANGE_PARTITION_TYPE=t
FDISK_SAVE_AND_QUIT=w
# Root Filesystem
FDISK_PARTITION_SELECT_ROOT=3
FDISK_PARTITION_TYPE_LINUX_ROOT=24
# Boot Filesystem EFI
FDISK_PARTITION_SELECT_EFI=2
FDISK_PARTITION_SIZE_EFI=+512M
FDISK_PARTITION_TYPE_EFI=1
# Boot Filesystem BIOS_BOOT
FDISK_PARTITION_SELECT_BIOS_BOOT=1
FDISK_PARTITION_SIZE_BIOS_BOOT=+1M
FDISK_PARTITION_TYPE_BIOS_BOOT=4

(
# GPT Partition Table is required
echo ${FDISK_CREATE_GPT_TABLE}
# Bios Partition
echo ${FDISK_CREATE_PARTITION}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_PARTITION_SIZE_BIOS_BOOT}
echo ${FDISK_CHANGE_PARTITION_TYPE}
echo ${FDISK_PARTITION_TYPE_BIOS_BOOT}
# EFI Partition
echo ${FDISK_CREATE_PARTITION}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_PARTITION_SIZE_EFI}
echo ${FDISK_CHANGE_PARTITION_TYPE}
echo ${FDISK_PARTITION_SELECT_EFI}
echo ${FDISK_PARTITION_TYPE_EFI}
# Root Partition
echo ${FDISK_CREATE_PARTITION}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_ACCEPT_DEFAULT}
echo ${FDISK_CHANGE_PARTITION_TYPE}
echo ${FDISK_PARTITION_SELECT_ROOT}
echo ${FDISK_PARTITION_TYPE_LINUX_ROOT}
# Save
echo ${FDISK_SAVE_AND_QUIT}
) | fdisk "${BLOCK_DEVICE_PATH}"

# ========================================= #
# ===== Get Partition Partition Paths ===== #
# ========================================= #
PARTITIONS=$(fdisk -l "${BLOCK_DEVICE_PATH}" | grep "^/" | awk '{print $1}')
PARTITION_EFI=$(echo ${PARTITIONS} | awk '{print $2}')
PARTITION_ROOT=$(echo ${PARTITIONS} | awk '{print $3}')
if [ "${PARTITION_EFI}" = "" ] || [ "${PARTITION_ROOT}" = "" ]; then
	echo "Something went wrong the the Partitions. EFI: [${PARTITION_EFI}] Root: [${PARTITION_ROOT}]"
	exit 1
fi

# ============================== #
# ===== Create Filesystems ===== #
# ============================== #
mkfs.msdos -F 32 "${PARTITION_EFI}"
mkfs.ext4 "${PARTITION_ROOT}"

# =========================================== #
# ===== Mount the new System under /mnt ===== #
# =========================================== #
mount "${PARTITION_ROOT}" /mnt
mkdir /mnt/boot
mount "${PARTITION_EFI}" /mnt/boot

# ================================= #
# ===== Install Base Packages ===== #
# ================================= #
pacstrap /mnt base base-devel linux-zen linux-zen-headers linux-firmware networkmanager grub efibootmgr intel-ucode amd-ucode rust netctl neovim git wget

# ===================================== #
# ===== Write System config Files ===== #
# ===================================== #
genfstab -U /mnt >> /mnt/etc/fstab
echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
echo "KEYMAP=de-latin1" > /mnt/etc/vconsole.conf
echo "arch" > /mnt/etc/hostname
echo "127.0.0.1 localhost" >> /mnt/etc/hosts
echo "::1 localhost" >> /mnt/etc/hosts
sed -i 's/#de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g;s/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /mnt/etc/locale.gen
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /mnt/etc/sudoers
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
sed -i 's/^HOOKS=.*/HOOKS=(base udev modconf block filesystems keyboard fsck)/g' /mnt/etc/mkinitcpio.conf

# ============================================== #
# ===== Executing required system commands ===== #
# ============================================== #
arch-chroot /mnt hwclock --systohc
arch-chroot /mnt locale-gen
arch-chroot /mnt systemctl enable NetworkManager
arch-chroot /mnt systemctl enable fstrim.timer
arch-chroot /mnt mkinitcpio -P

# =========================== #
# ===== User Management ===== #
# =========================== #
arch-chroot /mnt useradd --create-home ${USER_NAME}
arch-chroot /mnt usermod ${USER_NAME} -aG wheel
arch-chroot /mnt sh -c "echo -e '${USER_PASSWORD}\n${USER_PASSWORD}' | passwd ${USER_NAME}"
arch-chroot /mnt sh -c "echo -e '${USER_PASSWORD}\n${USER_PASSWORD}' | passwd"

# ============================== #
# ===== Install Bootloader ===== #
# ============================== #
# https://wiki.archlinux.org/index.php/Partitioning#Tricking_old_BIOS_into_booting_from_GPT
printf '\200\0\0\0\0\0\0\0\0\0\0\0\001\0\0\0' | dd of=${BLOCK_DEVICE_PATH} bs=1 seek=462
arch-chroot /mnt grub-install --target=i386-efi --efi-directory=/boot --bootloader-id=GRUB_i386-efi --no-nvram --removable
arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB_x86_64 --no-nvram --removable
arch-chroot /mnt grub-install --target=i386-pc ${BLOCK_DEVICE_PATH}
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

# ====================================================== #
# ===== Ask if desktop installer should be started ===== #
# ====================================================== #
echo "Cloning the desktop installer..."
arch-chroot /mnt runuser -l ${USER_NAME} -c "git clone https://gitlab.com/archx-rust/arch-desktop-installer ~/arch-desktop-installer"
echo "The installer has been downloaded to /home/${USER_NAME}/arch-desktop-installer."
echo "Reboot and execute 'cd /home/${USER_NAME}/arch-desktop-installer && cargo run --release' to start the installer."
